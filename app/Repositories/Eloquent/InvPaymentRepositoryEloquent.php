<?php
namespace App\Repositories\Eloquent;

use App\Repositories\Interfaces\InvPaymentRepositoryInterface;

use App\Models\Sales\InviPayOne;
use App\Models\Sales\InviPayTwo;
use App\Models\Sales\Payment;

class InvPaymentRepositoryEloquent implements InvPaymentRepositoryInterface
{
	protected $payone;

	protected $paytwo;

	protected $payment;

	public function __construct(InviPayOne $payone, InviPayTwo $paytwo, Payment $payment)
	{
		$this->payone = $payone;
		$this->paytwo = $paytwo;
		$this->payment = $payment;
	}

	public function ifInvPayOneExists($data)
	{
		$records = $this->payone->where('no',$data['no'])
			->where('date',$data['date'])
			->where('total_amount',$data['total_amount'])
			->where('member',$data['member'])
			->where('remark',$data['remark'])
			->where('reference',$data['reference'])
			->where('user',$data['user'])
			->where('time',$data['time'])
			->where('paytype',$data['paytype'])
			->count();
		if($records>0){
			return true;
		}else{
			return false;
		}
	}

	public function FirstOrCreatePayOne($row)
	{
		$records = $this->point_one->create([
			'no' => $row[1],
			'date' => $row[2],
			'total_amount' => $row[3],
			'member' => $row[4],
			'remark' => $row[5],
			'reference' => $row[6],
			'user' => $row[7],
			'time' => $row[8],
			'paytype' => $row[9],
			'discount' => $row[10],
			'interest' => $row[11],
			'tax' => $row[12],
			'location' => $row[13],
		]);
		return $records;
	}

	public function ifInvPayTwoExists($data)
	{
		$records = $this->paytwo->where('invi_pay_one_id')
			->where('no',$data['no'])
			->where('date',$data['date'])
			->where('time',$data['time'])
			->where('member',$data['member'])
			->where('account_no',$data['account_no'])
			->where('amount_due',$data['amount_due'])
			->where('sale_date',$data['sale_date'])
			->where('tseq_no',$data['tseq_no'])
			->where('remarks',$data['remarks'])
			->where('invoice_no',$data['invoice_no'])
			->count();
		if($records>0){
			return true;
		}else{
			return false;
		}
	}

	public function FirstOrCreatePayTwo($row, $data)
	{
		$records = $this->point_two->create([
			'invi_pay_one_id' => $data['invi_pay_one_id'],
			'no' => $row[1],
			'date' => $row[2],
			'time' => $row[3],
			'member' => $row[4],
			'account_no' => $row[5],
			'amount_due' => $row[6],
			'sale_date' => $row[7],
			'due_date' => $row[8],
			'tseq_no' => $row[9],
			'discount' => $row[10],
			'total_amount' => $row[11],
			'interest' => $row[12],
			'tax' => $row[13],
			'remarks' => $row[14],
			'location' => $row[15],
			'invoice_no' => $row[16],
			'so_deldate' => $row[17],
		]);
		return $records;
	}

	public function ifPaymentExists($data)
	{
		$records = $this->payment->where('no',$data['no'])
			->where('date',$data['date'])
			->where('amount',$data['amount'])
			->where('member',$data['member'])
			->where('user',$data['user'])
			->where('type',$data['type'])
			->where('tseq_no',$data['tseq_no'])
			->where('cmem_no',$data['cmem_no'])
			->where('pdc_date',$data['pdc_date'])
			->where('bank',$data['bank'])
			->where('purpose',$data['purpose'])
			->where('collected',$data['collected'])
			->where('invoice_no',$data['invoice_no'])
			->count();
		if($records>0){
			return true;
		}else{
			return false;
		}
	}

	public function FirstOrCreatePayment($row)
	{
		$records = $this->payment->create([
			'no' => $row[1],
			'date' => $row[2],
			'amount' => $row[3],
			'tax' => $row[4],
			'interest' => $row[5],
			'discount' => $row[6],
			'member' => $row[7],
			'reference' => $row[8],
			'user' => $row[9],
			'time' => $row[10],
			'type' => $row[11],
			'tseq_no' => $row[12],
			'cmem_no' => $row[13],
			'pdc_date' => $row[14],
			'location' => $row[15],
			'purpose' => $row[16],
			'bank' => $row[17],
			'so_deldate' => $row[18],
			'collected' => $row[19],
			'invoice_no' => $row[20],
		]);
		return $records;
	}
}