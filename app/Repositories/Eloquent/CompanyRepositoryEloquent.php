<?php
namespace App\Repositories\Eloquent;

use App\Repositories\Interfaces\CompanyRepositoryInterface;

use App\Models\Maintenance\Company;

class CompanyRepositoryEloquent implements CompanyRepositoryInterface
{
	protected $company;

	public function __construct(Company $company)
	{
		$this->company = $company;
	}

	public function ById($id)
	{
		return $this->company->find($id);
	}

	public function All()
	{
		return $this->company->all();
	}

	public function show($id)
	{
		return $this->company->findOrFail($id);
	}

	public function Update($postdata, $id)
	{
		if(!empty($id)){
			$this->ById($id)->fill($postdata)->save();
		}
	}

	public function Store($postdata)
	{
		if(!empty($postdata)){
			$this->company->fill($postdata)->save();
		}
	}

	public function Delete($id)
	{
		if(!empty($id)){
			$this->ById($id)->delete();
		}
	}
}