<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Interfaces\CompanyBranchRepositoryInterface;

use App\Models\Maintenance\CompanyBranch;

class CompanyBranchRepositoryEloquent implements CompanyBranchRepositoryInterface
{
	protected $branch;

	public function __construct(CompanyBranch $branch)
	{
		$this->branch = $branch;
	}

	public function All()
	{
		return $this->branch->all();
	}

	public function ById($id)
	{
		return $this->branch->find($id);
	}

	public function Show($id)
	{
		return $this->branch->findOrFail($id);
	}

	public function Store($postdata)
	{
		if(!empty($postdata)){
			return $this->branch->fill($postdata)->save();
		}
		return;
	}

	public function Update($postdata, $id)
	{
		if(!empty($postdata)){
			return $this->ById($id)->fill($postdata)->save();
		}
		return;
	}

	public function Delete($id)
	{
		if(!empty($id)){
			return $this->branch->findOrFail($id)->delete();
		}
		return;
	}
}