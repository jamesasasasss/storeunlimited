<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Interfaces\PointsRepositoryInterface;

use App\Models\Sales\PointOne;
use App\Models\Sales\PointTwo;
use App\Models\Sales\InviPayOne;
use App\Models\Sales\InviPayTwo;
use App\Models\Sales\Payment;

class PointsRepositoryEloquent implements PointsRepositoryInterface
{
	protected $point_one;

	protected $point_two;
	
	protected $pay_one;
	
	protected $pay_two;

	protected $payment;
	
	public function __construct(
		PointOne $point_one,
		PointTwo $point_two,
		InviPayOne $pay_one,
		InviPayTwo $pay_two,
		Payment $payment
	)
	{
		$this->point_one = $point_one;
		$this->point_two = $point_two;
		$this->pay_one = $pay_one;
		$this->pay_two = $pay_two;
		$this->payment = $payment;
	}

	public function ifExistPointOne($data)
	{
		$records = $this->point_one->where('tseq_no',$data['tseq_no'])
			->where('branch',$data['branch'])
			->where('sdump',$data['sdump'])
			->where('machine',$data['machine'])
			->where('reference',$data['reference'])
			->where('counter',$data['counter'])
			->where('personnel',$data['personnel'])
			->where('customer',$data['customer'])
			->count();
			if($records>0){
				return true;
			}else{
				return false;
			}
	}

	public function ifExistPointTwo($data)
	{
		$records = $this->point_two->where('point_one_id',$data['point_one_id'])
			->where('tseq_no',$data['tseq_no'])
			->where('barcode',$data['barcode'])
			->where('qty',$data['qty'])
			->where('category',$data['category'])
			->where('price',$data['price'])
			->where('branch',$data['branch'])
			->where('member',$data['member'])
			->where('corp_account',$data['corp_account'])
			->where('account_no',$data['account_no'])
			->where('personnel',$data['personnel'])
			->where('purpose',$data['purpose'])
			->where('customer',$data['customer'])
			->get();
			if(count($records)>0){
				return true;
			}else{
				return false;
			}
	}

	public function FirstOrCreatePointOne($row)
	{
		$records = $this->point_one->create([
			'tseq_no' => $row[1],
			'date' => $row[2],
			'time' => $row[3],
			'machine' => $row[4],
			'member' => $row[5],
			'price' => $row[6],
			'branch' => $row[7],
			'sdump' => $row[8],
			'location' => $row[9],
			'discount_a' => $row[10],
			'discount_p' => $row[11],
			'lc' => $row[12],
			'corp_account' => $row[13],
			'corp_amount' => $row[14],
			'account_no' => $row[15],
			'duedate' => $row[16],
			'paydate' => $row[17],
			'paid' => $row[18],
			'reference' => $row[19],
			'refund' => $row[20],
			'balance' => $row[21],
			'payment' => $row[22],
			'counter' => $row[23],
			'personnel' => $row[24],
			'term' => $row[25],
			'bonus' => $row[26],
			'purpose' => $row[27],
			'reason' => $row[28],
			'payment_type' => $row[29],
			'invoice_no' => $row[30],
			'so_deldate' => $row[31],
			'customer' => $row[32]
		]);
		return $records;
	}

	public function FirstOrCreatePointTwo($row, $data)
	{
		$records = $this->point_two->firstOrCreate([
			'point_one_id' => $data['point_one_id'],
			'tseq_no' => $row[1],
			'date' => $row[2],
			'time' => $row[3],
			'barcode' => $row[4],
			'qty' => $row[5],
			'category' => $row[6],
			'price' => $row[7],
			'amount' => $row[8],
			'machine' => $row[9],
			'branch' => $row[10],
			'sdump' => $row[11],
			'member' => $row[12],
			'location' => $row[13],
			'deduct' => $row[14],
			'discount_amount' => $row[15],
			'corp_amount' => $row[16],
			'corp_account' => $row[17],
			'account_no' => $row[18],
			'paydate' => $row[19],
			'paid' => $row[20],
			'reference' => $row[21],
			'lcash' => $row[22],
			'counter' => $row[23],
			'personnel' => $row[24],
			'bonus' => $row[25],
			'purpose' => $row[26],
			'description' => $row[27],
			'customer' => $row[28]
		]);
		return $records;
	}
}