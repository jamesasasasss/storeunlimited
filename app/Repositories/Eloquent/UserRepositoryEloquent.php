<?php
namespace App\Repositories\Eloquent;

use App\Repositories\Interfaces\UserRepositoryInterface;
use App\User;
use App\Models\Maintenance\Role;
use App\Models\Maintenance\RoleUser;
use Illuminate\Support\Facades\Hash;

class UserRepositoryEloquent implements UserRepositoryInterface
{
	protected $user;

	protected $role;

	protected $urole;

	public function __construct(User $user, Role $role, RoleUser $urole)
	{
		$this->user = $user;
		$this->role = $role;
		$this->urole = $urole;
	}

	public function ById($id)
	{
		return $this->user->find($id);
	}

	public function All()
	{
		return $this->user->all();
	}

	public function Store($request, $role)
	{
		if(!empty($request)){
			$new_user = new $this->user();
			$new_user->name = $request->name;
			$new_user->username = $request->username;
			$new_user->email = $request->email;
			$new_user->password = Hash::make($request->password);
			$new_user->active = 1;
			$new_user->save();

			if(!empty($role)){
				$new_role = new $this->urole();
				$new_role->user_id = $new_user->id;
				$new_role->role_id = $role;
				$new_role->save();
			}
			return;
		}
		return;
	}

	public function Show($id)
	{
		if(!empty($id)){
		 	return $this->user->findOrFail($id);
		}
	}

	public function Update($postdata, $id, $role)
	{
		if(!empty($id)){
			if(!empty($postdata)){
				$this->ById($id)->fill($postdata)->save();
			}
			$this->urole->where('user_id',$id)->delete();
			$new_role = new $this->urole();
			$new_role->user_id = $id;
			$new_role->role_id = $role;
			$new_role->save();

			return;
		}
		return;
	}

	public function Delete($id)
	{
		if(!empty($id)){
			$this->urole->where('user_id',$id)->delete();
			$this->ById($id)->delete();
			return;
		}
		return;
	}

	public function getRoles()
	{
		return $this->role->all();
	}

	public function StoreRole($role, $id)
	{
		$past_role = $this->urole->where('user_id',$id)->first();
		if(!empty($past_role)){
			$past_role->delete();
		}
		$new = new $this->urole();
		$new->user_id = $id;
		$new->role_id = $role;
		$new->save();

		return;
	}

	public function MyRole($id)
	{
		return $this->urole->where('user_id',$id)->first();
	}
}