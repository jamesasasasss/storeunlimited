<?php 
namespace App\Repositories\Eloquent;

use App\Repositories\Interfaces\UserBranchRepositoryInterface;

use App\Models\Maintenance\UserBranch;

class UserBranchRepositoryEloquent implements UserBranchRepositoryInterface
{
	protected $ubranch;

	public function __construct(UserBranch $ubranch)
	{
		$this->ubranch = $ubranch;
	}

	public function ById($id)
	{
		return $this->ubranch->where('user_id',$id)->get();
	}

	public function MyBranches($id)
	{
		$branches = $this->ubranch->where('user_id',$id)->get();
		$data = [];
		foreach ($branches as $branch) {
    		$data[] = $branch->branch_id;
    	}
    	return $data;
	}

	public function SearchAndDelete($id)
	{
		$branches = $this->ubranch->where('user_id',$id)->get();
		if(!empty($branches)){
			$this->ubranch->where('user_id',$id)->delete();
		}
		return;
	}

	public function SaveBranches($id, $branches)
	{
		if(!empty($branches)){
			foreach ($branches as $branch) {
				$my_branches = new $this->ubranch();
				$my_branches->branch_id = $branch;
				$my_branches->user_id = $id;
				$my_branches->save();
			}
			return;
		}
		return;
	}
}