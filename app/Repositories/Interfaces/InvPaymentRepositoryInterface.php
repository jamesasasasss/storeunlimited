<?php
namespace App\Repositories\Interfaces;

interface InvPaymentRepositoryInterface
{
	public function ifInvPayOneExists($data);

	public function ifInvPayTwoExists($data);

	public function ifPaymentExists($data);

	public function FirstOrCreatePayOne($row);

	public function FirstOrCreatePayTwo($row, $data);

	public function FirstOrCreatePayment($data);
}