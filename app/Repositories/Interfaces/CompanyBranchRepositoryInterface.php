<?php 
namespace App\Repositories\Interfaces;

interface CompanyBranchRepositoryInterface
{
	public function All();

	public function Store($postdata);

	public function Show($id);
	
	public function Delete($id);

	public function Update($postdata, $id);
}