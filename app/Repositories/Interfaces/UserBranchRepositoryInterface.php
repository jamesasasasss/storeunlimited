<?php 
namespace App\Repositories\Interfaces;

interface UserBranchRepositoryInterface
{
	public function ById($id);

	public function MyBranches($id);

	public function SearchAndDelete($id);

	public function SaveBranches($id, $branches);
}