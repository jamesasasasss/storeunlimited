<?php 
namespace App\Repositories\Interfaces;

interface CompanyRepositoryInterface
{
	public function All();

	public function show($id);

	public function Update($postdata, $id);

	public function Store($postdata);

	public function Delete($id);
}