<?php 
namespace App\Repositories\Interfaces;

interface UserRepositoryInterface
{
	public function All();

	public function getRoles();

	public function Store($request, $role);

	public function Show($id);

	public function Update($postdata, $id, $role);

	public function Delete($id);

	public function MyRole($id);
}