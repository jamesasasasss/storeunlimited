<?php 
namespace App\Repositories\Interfaces;

interface PointsRepositoryInterface
{
	public function ifExistPointOne($data);

	public function FirstOrCreatePointOne($row);

	public function ifExistPointTwo($data);

	public function FirstOrCreatePointTwo($row, $data);
}