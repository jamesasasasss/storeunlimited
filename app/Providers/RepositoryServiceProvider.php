<?php 

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
	public function register()
	{
		$this->app->bind(
			'App\Repositories\Interfaces\DashboardRepositoryInterface',
			'App\Repositories\Eloquent\DashboardRepositoryEloquent'
		);
		$this->app->bind(
			'App\Repositories\Interfaces\CompanyRepositoryInterface',
			'App\Repositories\Eloquent\CompanyRepositoryEloquent'
		);
		$this->app->bind(
			'App\Repositories\Interfaces\CompanyBranchRepositoryInterface',
			'App\Repositories\Eloquent\CompanyBranchRepositoryEloquent'
		);
		$this->app->bind(
			'App\Repositories\Interfaces\UserRepositoryInterface',
			'App\Repositories\Eloquent\UserRepositoryEloquent'
		);
		$this->app->bind(
			'App\Repositories\Interfaces\UserBranchRepositoryInterface',
			'App\Repositories\Eloquent\UserBranchRepositoryEloquent'
		);
		$this->app->bind(
			'App\Repositories\Interfaces\PointsRepositoryInterface',
			'App\Repositories\Eloquent\PointsRepositoryEloquent'
		);
		$this->app->bind(
			'App\Repositories\Interfaces\InvPaymentRepositoryInterface',
			'App\Repositories\Eloquent\InvPaymentRepositoryEloquent'
		);
	}
}