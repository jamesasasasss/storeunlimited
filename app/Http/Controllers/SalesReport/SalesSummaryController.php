<?php

namespace App\Http\Controllers\SalesReport;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
class SalesSummaryController extends Controller
{
	protected $request;

	public function __construct(Request $request)
	{
		$this->request = $request;
	}

    public function index()
    {
    	if(empty(Auth::user())){
    		return redirect('/')->with('session_expired', 'Logout');
    	}
    	$date_from = date('m/d/Y');
    	$date_to = date('m/d/Y');
    	return view('sales.index',compact('date_from','date_to'));
    }
}
