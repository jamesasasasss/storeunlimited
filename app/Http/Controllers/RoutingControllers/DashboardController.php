<?php

namespace App\Http\Controllers\RoutingControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use App\Repositories\Interface\DashboardRepositoryInterface;
use Auth;

class DashboardController extends Controller
{
    public function index()
    {
    	if(empty(Auth::user())){
    		return redirect('/')->with('session_expired', 'Logout');
    	}
    	return view('dashboard.index');
    }
}
