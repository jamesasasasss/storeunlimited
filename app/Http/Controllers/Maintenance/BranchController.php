<?php

namespace App\Http\Controllers\Maintenance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\Interfaces\CompanyBranchRepositoryInterface;

use Auth;

class BranchController extends Controller
{
	protected $branch;
	protected $request;
	public function __construct(CompanyBranchRepositoryInterface $branch, Request $request)
	{
		$this->branch = $branch;
		$this->request = $request;
	}

    public function index()
    {
        if(empty(Auth::user())){
            return redirect('/')->with('session_expired', 'Logout');
        }
    	$branches = $this->branch->All();

    	return view('branch.index', compact('branches'));
    }

    public function add()
    {
    	return view('branch.create');
    }

    public function store(Request $request)
    {
    	if($request->area == '' || $request->area == null){
    		$area = 0.00;
    	}else{
    		$area = $request->area;
    	}
    	$postdata['company_id'] = $request->company_id;
    	$postdata['branch_code'] = $request->branch_code;
    	$postdata['branch_name'] = strtoupper($request->branch_name);
    	$postdata['area'] = $area;

    	$this->branch->Store($postdata);

    	return redirect('branch')->with('is_added', 'Success');
    }

    public function show($id)
    {
    	$branch = $this->branch->Show($id);

    	return view('branch.edit',compact('branch'));
    }

    public function update(Request $request, $id)
    {
    	if($request->area == '' || $request->area == null){
    		$area = 0.00;
    	}else{
    		$area = $request->area;
    	}
    	$postdata['company_id'] = $request->company_id;
    	$postdata['branch_name'] = strtoupper($request->branch_name) ;
    	$postdata['branch_code'] = $request->branch_code;
    	$postdata['area'] = $area;

    	$this->branch->Update($postdata, $id);

    	return redirect('branch')->with('is_success', 'Success');
    }

    public function delete(Request $request)
    {
    	$id = $request->id;

    	$this->branch->Delete($id);

    	return redirect('branch')->with('is_deleted', 'Success');
    }
}
