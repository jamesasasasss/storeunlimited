<?php

namespace App\Http\Controllers\Maintenance;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Repositories\Interfaces\UserBranchRepositoryInterface;
use App\Repositories\Interfaces\CompanyBranchRepositoryInterface;

use Auth;

class UserController extends Controller
{
	protected $user;

    protected $request;

    protected $ubranch;

	protected $branch;

    public function __construct(
        UserRepositoryInterface $user, 
        UserBranchRepositoryInterface $ubranch, 
        CompanyBranchRepositoryInterface $branch,
        Request $request)
    {
    	$this->user = $user;
        $this->ubranch = $ubranch;
        $this->branch = $branch;
    	$this->request = $request;
    }

    public function index()
    {   
        if(empty(Auth::user())){
            return redirect('/')->with('session_expired', 'Logout');
        }
    	$users = $this->user->All();

    	return view('user.index', compact('users'));
    }

    public function add()
    {
        $roles = $this->user->getRoles();
    	return view('user.create',compact('roles'));
    }

    public function store(Request $request)
    {
    	if(!empty($request)){
            $this->user->Store($request, $request->role_id);
    		return redirect('users')->with('is_added', 'Success');
    	}
    }

    public function show($id)
    {
    	$user = $this->user->Show($id);
        $my_role = $this->user->MyRole($id);
        $roles = $this->user->getRoles();
    	return view('user.edit',compact('user','roles','my_role'));
    }

    public function update(Request $request, $id)
    {
    	if(!empty($request)){
    		$postdata['name'] = $request->name;
    		$postdata['username'] = $request->username;
    		$postdata['email'] = $request->email;

    		$this->user->Update($postdata, $id, $request->role_id);

    		return redirect('users')->with('is_success', 'Success');
    	}
    }

    public function delete(Request $request)
    {
    	$id = $request->id;

    	$this->user->Delete($id);

    	return redirect()->back()->with('is_deleted', 'Success');
    }

    public function branches($id)
    {
        $id = $id;

        $my_branches = $this->ubranch->MyBranches($id);

        $branches = $this->branch->All();

        $user = $id;

        return view('user.branch',compact('my_branches','branches','user'));
    }

    public function add_branch(Request $request)
    {
        $this->ubranch->SearchAndDelete($request->user_id);

        $this->ubranch->SaveBranches($request->user_id, $request->branch);
        
        return redirect()->back()->with('is_success', 'Success');
    }
}
