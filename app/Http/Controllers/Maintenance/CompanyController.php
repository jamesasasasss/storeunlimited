<?php

namespace App\Http\Controllers\Maintenance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\Interfaces\CompanyRepositoryInterface;

use Auth;

class CompanyController extends Controller
{
	protected $company;
	protected $request;

	public function __construct(CompanyRepositoryInterface $company, Request $request)
	{
		$this->request = $request;
		$this->company = $company;
	}
    public function index()
    {
        if(empty(Auth::user())){
            return redirect('/')->with('session_expired', 'Logout');
        }
    	$companies = $this->company->All();

    	return view('company.index',compact('companies'));
    }
    public function show($id)
    {
    	$cmp = $this->company->show($id);

    	return view('company.edit',compact('cmp'));
    }
    public function update(Request $request, $id)
    {
    	$postdata['company_code'] = $request->company_code;
    	$postdata['company_name'] = $request->company_name;

    	$this->company->Update($postdata, $id);

    	return redirect('company')->with('is_success', 'Success');
    }

    public function add()
    {
    	return view('company.create');
    }

    public function store(Request $request)
    {
    	$postdata['company_code'] = $request->company_code;
    	$postdata['company_name'] = $request->company_name;

    	$this->company->Store($postdata);

    	return redirect('company')->with('is_added', 'Sucess');
    }

    public function delete(Request $request)
    {
    	$id = $request->id;

    	$this->company->Delete($id);

    	return redirect('company')->with('is_deleted', 'Success');
    }
}
