<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Illuminate\Http\Request;
use DB;

use App\Repositories\Interfaces\PointsRepositoryInterface;

class PointsController extends Controller
{
	protected $request;

	protected $trans;

    public function __construct(Request $request, PointsRepositoryInterface $trans)
    {
    	$this->request = $request;
    	$this->trans = $trans;
    }

    public function upload(Request $request)
    {
        $fileName = $request->file('data')->getClientOriginalName();

        $c_folder = substr($fileName, 0,4);
        $b_folder = substr($fileName, 4,4);
        $t_folder = substr($fileName, 8,2);	
        $destinationPath = storage_path().'/uploads/sales/'.$c_folder.'/'.$b_folder.'/'.$t_folder;

        if(!\File::exists($destinationPath))
        {
            mkdir($destinationPath, 0755, true);
        }

        $request->file('data')->move($destinationPath, $fileName);

        $filePath = $destinationPath."/".$fileName;

        DB::beginTransaction();
        try
        {
            $reader = ReaderFactory::create(Type::CSV); // for XLSX files
            $reader->setFieldDelimiter('|');
            $reader->open($filePath);
            foreach ($reader->getSheetIterator() as $sheet) {
                $last_id = 0;
                $transaction_no = 0;
                foreach ($sheet->getRowIterator() as $row) {
                    if($row[0] == 'H'){
                        $data['tseq_no'] = $row[1];
                        $data['branch'] = $row[7];
                        $data['date'] = $row[2];
                        $data['sdump'] = $row[8];
                        $data['location'] = $row[9];
                        $data['time'] = $row[3];
                        $data['machine'] = $row[4];
                        $data['reference'] = $row[19];
                        $data['counter'] = $row[23];
                        $data['personnel'] = $row[24];
                        $data['term'] = $row[25];
                        $data['bonus'] = $row[26];
                        $data['customer'] = $row[32];
                        if(!$this->trans->ifExistPointOne($data)){
                            $point1 = $this->trans->FirstOrCreatePointOne($row);
                            $points1_id = $point1->id;
                            $tseq_no = $row[1];
                            $new = true;
                        }else{
                            $new = false;
                        }
                    }
                    if($new){
                        if($row[0] == 'D'){
                            $data['point_one_id'] = $points1_id;

                            $data['tseq_no'] = $tseq_no;
                            $data['barcode'] = $row[5];
                            $data['qty'] = $row[6];
                            $data['category'] = $row[7];
                            $data['price'] = $row[8];
                            $data['branch'] = $row[11];
                            $data['member'] = $row[13];
                            $data['corp_account'] = $row[18];
                            $data['account_no'] = $row[19];
                            $data['personnel'] = $row[25];
                            $data['purpose'] = $row[27];
                            $data['customer'] = $row[29];

                            $detail = $this->trans->ifExistPointTwo($data);
                            if(empty($detail)){
                                $this->trans->FirstOrCreatePointTwo($row, $data);
                            }                      
                        }
                    }
                }
            }
            $reader->close();
            DB::commit();
            return response()->json(array('msg' => 'file uploaded', 'status' => 0));
        }catch (\Exception $e){
            DB::rollback();
            return response()->json(array('msg' => 'file upload error', 'status' => 1));
        }
    }
}
