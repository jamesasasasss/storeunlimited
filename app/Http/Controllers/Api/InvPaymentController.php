<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use DB;

use App\Repositories\Interfaces\InvPaymentRepositoryInterface;

class InvPaymentController extends Controller
{
	protected $payment;

	protected $request;

    public function __construct(InvPaymentRepositoryInterface $payment, Request $request)
    {
    	$this->payment = $payment;
    	$this->request = $request;
    }

    public function upload(Request $request)
    {
    	$fileName = $request->file('data')->getClientOriginalName();

        $c_folder = substr($fileName, 0,4);
        $b_folder = substr($fileName, 4,4);
        $t_folder = substr($fileName, 8,2);	
        $destinationPath = storage_path().'/uploads/payment/'.$c_folder.'/'.$b_folder.'/'.$t_folder;

        if(!\File::exists($destinationPath))
        {
            mkdir($destinationPath, 0755, true);
        }

        $request->file('data')->move($destinationPath, $fileName);

        $filePath = $destinationPath."/".$fileName;

        DB::beginTransaction();
        try
        {
            $reader = ReaderFactory::create(Type::CSV); // for XLSX files
            $reader->setFieldDelimiter('|');
            $reader->open($filePath);
            foreach ($reader->getSheetIterator() as $sheet) {
                $last_id = 0;
                $transaction_no = 0;
                foreach ($sheet->getRowIterator() as $row) {
                    if($row[0] == 'H'){
                        $data['no'] = $row[1];
                        $data['date'] = $row[2];
                        $data['total_amount'] = $row[3];
                        $data['member'] = $row[4];
                        $data['remark'] = $row[5];
                        $data['reference'] = $row[6];
                        $data['user'] = $row[7];
                        $data['time'] = $row[8];
                        $data['paytype'] = $row[9];

                        if(!$this->payment->ifInvPayOneExists($data)){
                            $payone = $this->payment->FirstOrCreatePayOne($row);
                            $payone_id = $payone->id;
                    	}
	                    if($row[0] == 'D'){
	                        $data['invi_pay_one_id'] = $payone_id;

	                        $data['no'] = $row[1];
	                        $data['date'] = $row[2];
	                        $data['time'] = $row[3];
	                        $data['member'] = $row[4];
	                        $data['account_no'] = $row[5];
	                        $data['amount_due'] = $row[6];
	                        $data['sale_date'] = $row[7];
	                        $data['tseq_no'] = $row[9];
	                        $data['remarks'] = $row[15];
	                        $data['invoice_no'] = $row[16];

	                        $detail = $this->payment->ifInvPayTwoExists($data);
	                        if(empty($detail)){
	                            $this->payment->FirstOrCreatePayTwo($row, $data);
	                        }                      
	                    }
	                    if($row[0] == 'P'){
	                    	$data['no'] = $row[1];
	                        $data['date'] = $row[2];
	                        $data['amount'] = $row[3];
	                        $data['member'] = $row[7];
	                        $data['user'] = $row[9];
	                        $data['type'] = $row[11];
	                        $data['tseq_no'] = $row[12];
	                        $data['cmem_no'] = $row[13];
	                        $data['pdc_date'] = $row[14];
	                        $data['bank'] = $row[17];
	                        $data['purpose'] = $row[16];
	                        $data['collected'] = $row[19];
	                        $data['invoice_no'] = $row[20];

	                        $pay = $this->payment->ifPaymentExists($data);
	                        if(empty($pay)){
	                        	$this->payment->FirstOrCreatePayment($row);
	                        }
	                    }
                    }
                }
            }
            $reader->close();
            DB::commit();
            return response()->json(array('msg' => 'file uploaded', 'status' => 0));
        }catch (\Exception $e){
            DB::rollback();
            return response()->json(array('msg' => 'file upload error', 'status' => 1));
        }
    }
}
