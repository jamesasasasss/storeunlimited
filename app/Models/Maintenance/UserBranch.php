<?php

namespace App\Models\Maintenance;

use Illuminate\Database\Eloquent\Model;

class UserBranch extends Model
{
    protected $table = 'user_branches';
    
    protected $fillable = ['branch_id','user_id'];
}
