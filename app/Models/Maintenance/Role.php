<?php

namespace App\Models\Maintenance;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';

    protected $fillable = [
    	'role_name',
		'role_display_name',
		'description'
    ];
}
