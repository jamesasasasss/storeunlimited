<?php

namespace App\Models\Maintenance;

use Illuminate\Database\Eloquent\Model;

class CompanyBranch extends Model
{
    protected $table = 'company_branches';
    protected $fillable = ['company_id', 'branch_code', 'branch_name', 'area'];

    public function company()
    {
    	return $this->hasOne('App\Models\Maintenance\Company', 'id', 'company_id');
    }
}
