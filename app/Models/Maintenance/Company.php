<?php

namespace App\Models\Maintenance;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';
    protected $fillable = ['company_code', 'company_name'];
}
