<?php

namespace App\Models\Sales;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';

    protected $fillable = [
		'no','date',
		'amount','tax',
		'interest','discount',
		'member','reference',
		'user','time',
		'type','tseq_no',
		'cmem_no','pdc_date',
		'location','purpose',
		'bank','so_deldate',
		'collected','invoice_no'
    ];
}
