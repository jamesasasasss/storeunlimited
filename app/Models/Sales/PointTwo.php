<?php

namespace App\Models\Sales;

use Illuminate\Database\Eloquent\Model;

class PointTwo extends Model
{
    protected $table = 'point_twos';
    protected $fillable = [
    	'tseq_no','date',
		'time','barcode',
		'qty','category',
		'price','amount',
		'machine','branch',
		'sdump','member',
		'location','deduct',
		'discount_amount','corp_account',
		'account_no','paydate',
		'paid','reference',
		'lcash','counter',
		'personnel','bonus',
		'purpose','description',
		'customer','point_one_id'
    ];
}
