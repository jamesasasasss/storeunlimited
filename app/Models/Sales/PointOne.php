<?php

namespace App\Models\Sales;

use Illuminate\Database\Eloquent\Model;

class PointOne extends Model
{
    protected $table = 'point_ones';
    protected $fillable = [
		'tseq_no',
		'date','time',
		'machine','price',
		'branch','sdump',
		'location','discount_a',
		'discount_p','lc',
		'corp_account','corp_amount',
		'account_no','duedate',
		'paydate','paid',
		'reference','refund',
		'balance','payment',
		'counter','personnel',
		'term','bonus',
		'purpose','reason',
		'payment_type','invoice_no',
		'so_deldate','customer'
    ];
}
