<?php

namespace App\Models\Sales;

use Illuminate\Database\Eloquent\Model;

class InviPayOne extends Model
{
    protected $table = 'invi_pay_ones';

    protected $fillalbe = [
		'no',
		'date','total_amount',
		'member','remark',
		'reference','user',
		'time','paytype',
		'discount','interest',
		'tax','location'
    ];
}
