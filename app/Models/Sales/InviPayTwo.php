<?php

namespace App\Models\Sales;

use Illuminate\Database\Eloquent\Model;

class InviPayTwo extends Model
{
    protected $table = 'invi_pay_two';

    protected $fillable = [
    	'no',
		'date','time',
		'member','account_no',
		'amount_due','sale_date',
		'due_date','tseq_no',
		'discount','total_amount',
		'interest','tax',
		'remarks','location',
		'invoice_no','so_deldate'
    ];
}
