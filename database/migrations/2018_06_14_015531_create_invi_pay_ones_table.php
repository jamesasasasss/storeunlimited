<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInviPayOnesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invi_pay_ones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no')->nullable();
            $table->date('date')->nullable();
            $table->decimal('total_amount',12,2)->nullable();
            $table->string('member')->nullable();
            $table->string('remark')->nullable();
            $table->string('reference')->nullable();
            $table->string('user')->nullable();
            $table->string('time')->nullable();
            $table->string('paytype')->nullable();
            $table->string('discount')->nullable();
            $table->string('interest')->nullable();
            $table->decimal('tax',12,2)->nullable();
            $table->string('location')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invi_pay_ones');
    }
}
