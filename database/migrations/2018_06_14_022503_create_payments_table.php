<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no');
            $table->date('date')->nullable();
            $table->decimal('amount',12,2)->nullable();
            $table->string('tax')->nullable();
            $table->string('interest')->nullable();
            $table->string('discount')->nullable();
            $table->string('member')->nullable();
            $table->string('reference')->nullable();
            $table->string('user')->nullable();
            $table->string('time')->nullable();
            $table->string('type')->nullable();
            $table->string('tseq_no')->nullable();
            $table->string('cmem_no')->nullable();
            $table->date('pdc_date')->nullable();
            $table->string('location')->nullable();
            $table->string('purpose')->nullable();
            $table->string('bank')->nullable();
            $table->string('so_deldate')->nullable();
            $table->string('collected')->nullable();
            $table->string('invoice_no')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
