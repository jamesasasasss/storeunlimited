<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInviPayTwosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invi_pay_twos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no')->nullable();
            $table->date('date')->nullable();
            $table->string('time')->nullable();
            $table->string('member')->nullable();
            $table->string('account_no')->nullable();
            $table->decimal('amount_due',12,2)->nullable();
            $table->date('sale_date')->nullable();
            $table->date('due_date')->nullable();
            $table->string('tseq_no')->nullable();
            $table->string('discount')->nullable();
            $table->decimal('total_amount',12,2)->nullable();
            $table->string('interest')->nullable();
            $table->string('tax')->nullable();
            $table->string('remarks')->nullable();
            $table->string('location')->nullable();
            $table->string('invoice_no')->nullable();
            $table->string('so_deldate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invi_pay_twos');
    }
}
