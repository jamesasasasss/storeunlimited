<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointTwosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_twos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tseq_no');
            $table->date('date');
            $table->string('time');
            $table->string('barcode');
            $table->integer('qty');
            $table->string('category')->nullable();
            $table->decimal('price',12,2)->nullable();
            $table->decimal('amount',12,2)->nullable();
            $table->string('machine')->nullable();
            $table->string('branch');
            $table->string('sdump')->nullable();
            $table->string('member')->nullable();
            $table->string('location')->nullable();
            $table->decimal('deduct',12,2)->nullable();
            $table->decimal('discount_amount',12,2)->nullable();
            $table->string('corp_account')->nullable();
            $table->string('account_no')->nullable();
            $table->date('paydate')->nullable();
            $table->string('paid')->nullable();
            $table->string('reference')->nullable();
            $table->decimal('lcash',12,2)->nullable();
            $table->string('counter')->nullable();
            $table->string('personnel')->nullable();
            $table->string('bonus')->nullable();
            $table->string('purpose')->nullable();
            $table->string('description')->nullable();
            $table->string('customer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_twos');
    }
}
