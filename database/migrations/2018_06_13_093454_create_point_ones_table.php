<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointOnesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_ones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tseq_no');
            $table->date('date');
            $table->string('time');
            $table->string('machine')->nullable();
            $table->decimal('price',12,2);
            $table->string('branch');
            $table->string('sdump')->nullable();
            $table->string('location');
            $table->decimal('discount_a',12,2)->nullable();
            $table->decimal('discount_p',12,2)->nullable();
            $table->string('lc')->nullable();
            $table->string('corp_account')->nullable();
            $table->decimal('corp_amount',12,2)->nullable();
            $table->string('account_no')->nullable();
            $table->date('duedate')->nullable();
            $table->date('paydate')->nullable();
            $table->string('paid')->nullable();
            $table->string('reference')->nullable();
            $table->decimal('refund',12,2)->nullable();
            $table->decimal('balance',12,2)->nullable();
            $table->string('payment')->nullable();
            $table->string('counter')->nullable();
            $table->string('personnel')->nullable();
            $table->string('term')->nullable();
            $table->string('bonus')->nullable();
            $table->string('purpose')->nullable();
            $table->string('reason')->nullable();
            $table->string('payment_type')->nullable();
            $table->string('invoice_no')->nullable();
            $table->string('so_deldate')->nullable();
            $table->string('customer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_ones');
    }
}
