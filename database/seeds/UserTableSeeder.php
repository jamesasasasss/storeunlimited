<?php

use Illuminate\Database\Seeder;

use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        User::create([
            'name' => 'admin',
            'username' => 'admin',
            'email' => 'admin@chasetech.com',
            'password' => bcrypt('password'),
            'active' => '1',
            'created_at' => '2017-05-12 05:57:02',
            'updated_at' => '2017-05-12 05:57:02'
        ]);

        DB::table('roles')->truncate();
        DB::table('roles')->insert([
        	'role_name' => 'admin',
        	'role_display_name' => 'ADMINISTRATOR',
        	'description' => 'Web Administrator',
        ]);

        DB::table('roles')->insert([
        	'role_name' => 'fortunewear',
        	'role_display_name' => 'FORTUNEWEAR',
        	'description' => 'FortuneWear'
        ]);

        DB::table('role_users')->truncate();

        DB::table('role_users')->insert([
        	'user_id' => '1',
        	'role_id' => '1'
        ]);
    }
}
