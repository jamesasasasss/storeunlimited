<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
// Dashboard
Route::resource('dashboard', 'RoutingControllers\DashboardController');
// Company
Route::resource('company', 'Maintenance\CompanyController');
Route::get('company/create',['as'=>'company.add','uses'=>'Maintenance\CompanyController@add']);
Route::post('company/delete',['as'=>'company.delete','uses'=>'Maintenance\CompanyController@delete']);
// Branches
Route::resource('branch', 'Maintenance\BranchController');
Route::get('branch/create',['as'=>'branch.add','uses'=>'Maintenance\BranchController@add']);
Route::post('branch/delete',['as'=>'branch.delete','uses'=>'Maintenance\BranchController@delete']);
// Users
Route::resource('users', 'Maintenance\UserController');
Route::get('users/create',['as'=>'users.add','uses'=>'Maintenance\UserController@add']);
Route::post('users/delete',['as'=>'users.delete','uses'=>"Maintenance\UserController@delete"]);
Route::get('users/branches/{id}',['as'=>'users.branches','uses'=>'Maintenance\UserController@branches']);
Route::post('users/addbranch',['as'=>'users.add_branch','uses'=>'Maintenance\UserController@add_branch']);
// Sales
Route::resource('sales', 'SalesReport\SalesSummaryController');
