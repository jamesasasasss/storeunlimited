@extends('layouts.master')
@section('main_body')
<div class="row">
    <div class="col-lg-12 animated flash">
        <?php if (session('is_success')): ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
                <center><p style="color:white">Branches were successfully tagged to this user!<i class="fa fa-check"></i></p></center>                
            </div>
        <?php endif;?>
        <?php if (session('is_added')): ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
                <center><p style="color:white">User was successfully added!<i class="fa fa-check"></i></p></center>                
            </div>
        <?php endif;?>
        <?php if (session('is_deleted')): ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
                <center><p style="color:white">User was successfully deleted!<i class="fa fa-check"></i></p></center>                
            </div>
        <?php endif;?>
    </div>
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="btn-group float-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                    <li class="breadcrumb-item"><a href="#">Fortune Wear</a></li>
                    <li class="breadcrumb-item active">User Branches</li>
                </ol>
            </div>
            <h4 class="page-title">User Branches</h4>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="row">
	<div class="col-12">
        <div class="card m-b-30">
            <div class="card-body">            
                <h4 class="mt-0 header-title"></h4>      
                {{Form::open(array('route'=>'users.add_branch', 'method'=>'POST'))}}
                <table id="datatable" class="table table-bordered">
                	<thead>
                		<tr>
                			<th>Company</th>
                            <th>Branch Code</th>
                			<th>Branch Name</th>
                			<th>Action</th>
                		</tr>
                	</thead>
                	<tbody>
                		@foreach($branches as $branch)
                		<tr>
                            <td>{{$branch->company->company_name}}</td>
                			<td>{{$branch->branch_code}}</td>
                            <td>{{$branch->branch_name}}</td>
                            <td>
                                {{ Form::checkbox('branch[]', $branch->id, (in_array($branch->id, $my_branches) ? true : false)) }}
                            </td>
                		</tr>
                		@endforeach
                	</tbody>
                </table>
                {{ Form::hidden('user_id', $user) }}
                <button type="submit" class="btn btn-xs btn-primary pull-right"> Save</button>
                <a href="{{route('users.index')}}" class="btn btn-xs btn-danger pull-right"> Cancel</a>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection