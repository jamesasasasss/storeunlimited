@extends('layouts.master')
@section('main_body')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="btn-group float-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                    <li class="breadcrumb-item"><a href="#">Fortune Wear</a></li>
                    <li class="breadcrumb-item active">Users</li>
                </ol>
            </div>
            <h4 class="page-title">Add User</h4>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="row">
    <div class="col-6">
        <div class="card m-b-30">
            <div class="card-body">            
                <h4 class="mt-0 header-title"></h4>
            </div>
            {{ Form::open(array('route' => 'users.store', 'method' => 'POST', 'class' => 'form-horizontal form-label-left')) }}
            <div class="col-md-12">
                <div class="form-group">
                    {{Form::label('name','Name Of User')}}
                    <input type="text" class="form-control" name="name" required="">
                </div>
                <div class="form-group">
                    {{Form::label('username','Username')}}
                    <input type="text" class="form-control" name="username" required="">
                </div>
                <div class="form-group">
                    {{Form::label('email','Email')}}
                    <input type="email" class="form-control" name="email" required="">
                </div>
                <div class="form-group">
                    {{Form::label('acc_type','Select Role Of Account')}}
                    <select name="role_id" class="form-control" required="">
                        <option value=""> </option>
                        @foreach($roles as $role)
                        <option value="{{$role->id}}">{{strtoupper($role->role_name)}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    {{Form::label('pass','Password')}}
                    <input type="password" class="form-control" name="password" required="">
                </div>
                <div class="form-group">
                    <button class="btn btn-primary btn-xs" type="submit"> Save</button>
                    <a href="{{route('users.index')}}" class="btn btn-xs btn-warning"> Cancel</a>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>
@endsection