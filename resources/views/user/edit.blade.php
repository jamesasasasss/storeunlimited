@extends('layouts.master')
@section('main_body')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="btn-group float-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                    <li class="breadcrumb-item"><a href="#">Fortune Wear</a></li>
                    <li class="breadcrumb-item active">Users</li>
                </ol>
            </div>
            <h4 class="page-title">User Details</h4>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="row">
    <div class="col-6">
        <div class="card m-b-30">
            <div class="card-body">            
                <h4 class="mt-0 header-title"></h4>
            </div>
            {{ Form::open(array('route' => array('users.update',$user->id), 'method' => 'PUT', 'class' => 'form-horizontal form-label-left')) }}
            <div class="col-md-12">
                <div class="form-group">
                    {{Form::label('name','Name Of User')}}
                    <input type="text" class="form-control" name="name" required="" value="{{$user->name}}">
                </div>
                <div class="form-group">
                    {{Form::label('username','Username')}}
                    <input type="text" class="form-control" name="username" required="" value="{{$user->username}}">
                </div>
                <div class="form-group">
                    {{Form::label('email','Email')}}
                    <input type="email" class="form-control" name="email" required="" value="{{$user->email}}">
                </div>
                <div class="form-group">
                    {{Form::label('acc_type','Role Of Account')}}
                    <select name="role_id" class="form-control">
                        @foreach($roles as $role)
                        @if($role->id == $my_role->role_id)
                        <option value="{{$role->id}}" selected="">{{strtoupper($role->role_name)}}</option>
                        @else
                        <option value="{{$role->id}}">{{strtoupper($role->role_name)}}</option>
                        @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary btn-xs" type="submit"> Save</button>
                    <a href="{{route('users.index')}}" class="btn btn-xs btn-warning"> Cancel</a>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>
@endsection