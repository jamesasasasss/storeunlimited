@extends('layouts.master')
@section('main_body')
@push('scripts')
    <script src="/assets/scripts/salesreport.js"></script>
@endpush
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="btn-group float-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                    <li class="breadcrumb-item"><a href="#">Fortune Wear</a></li>
                    <li class="breadcrumb-item active">Sales Report</li>
                </ol>
            </div>
            <h4 class="page-title">Sales Report</h4>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card m-b-30">
            <div class="card-body">
                <div class="col-md-3 col-xs-12">
                    <div class="form-group">
                        <label for="date_from">Date From</label>
                        {!! Form::text('date_from',$date_from,array('class' => 'form-control', 'id' => 'date_from')) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection