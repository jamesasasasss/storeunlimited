<div class="topbar">
    <nav class="navbar-custom">
        <div class="dropdown notification-list nav-pro-img">
            <div class="list-inline-item hide-phone app-search">
                <form role="search" class="">
                    <div class="form-group pt-1">
                    </div>
                </form>
            </div>
        </div>
        <ul class="list-inline float-right mb-0 mr-3">
            <!-- language-->
            <li class="list-inline-item dropdown notification-list">
                <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false"
                    aria-expanded="false">
                    <span class="float-right" style="color: white;">
                        <i class="fa fa-user-circle fa-2x"></i>
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                    <div class="dropdown-item noti-title">
                        <h5>
                            Welcome {{strtoupper(Auth::user()->username)}}
                        </h5>
                    </div>
                    <a class="dropdown-item" style="cursor: pointer;" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
        <ul class="list-inline menu-left mb-0">
            <li class="float-left">
                <button class="button-menu-mobile open-left waves-light waves-effect">
                    <i class="mdi mdi-menu"></i>
                </button>
            </li>
        </ul>
        <div class="clearfix"></div>
    </nav>
</div>