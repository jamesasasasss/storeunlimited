<div class="sidebar-inner slimscrollleft" id="sidebar-main">

                    <div id="sidebar-menu">
                        <ul>
                            <li class="menu-title">Main</li>

                            <li>
                                <a href="{{route('dashboard.index')}}" class="waves-effect">
                                    <i class="fa fa-tachometer-alt"></i>
                                    <span> Dashboard</span>
                                </a>
                            </li>
                            @if(Auth::user()->role->role_id == 1)
                            <li class="menu-title">Maintenance</li>
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect">
                                    <i class="fa fa-sliders-h"></i>
                                    <span> Maintenance </span>
                                    <span class="float-right">
                                        <i class="mdi mdi-chevron-right"></i>
                                    </span>
                                </a>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="{{route('users.index')}}" class="waves-effect">
                                            <i class="fa fa-users"></i>
                                            <span> Users </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('branch.index')}}" class="waves-effect">
                                            <i class="fa fa-sitemap"></i>
                                            <span> Branches </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('company.index')}}" class="waves-effect">
                                            <i class="fa fa-building"></i>
                                            <span> Companies </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            @endif
                            <li class="menu-title">Sales</li>
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect">
                                    <i class="fa fa-copy"></i>
                                    <span> Sales Report </span>
                                    <span class="float-right">
                                        <i class="mdi mdi-chevron-right"></i>
                                    </span>
                                </a>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="{{route('sales.index')}}" class="waves-effect">
                                            <i class="fa fa-file"></i>
                                            <span> Sales </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect">
                                    <i class="mdi mdi-cards"></i>
                                    <span> Forms </span>
                                    <span class="badge badge-pill badge-info float-right">8</span>
                                </a>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="form-elements.html">Form Elements</a>
                                    </li>
                                    <li>
                                        <a href="form-validation.html">Form Validation</a>
                                    </li>
                                    <li>
                                        <a href="form-advanced.html">Form Advanced</a>
                                    </li>
                                    <li>
                                        <a href="form-mask.html">Form Mask</a>
                                    </li>
                                    <li>
                                        <a href="form-editors.html">Form Editors</a>
                                    </li>
                                    <li>
                                        <a href="form-uploads.html">Form File Upload</a>
                                    </li>
                                </ul>
                            </li> -->
                            <!-- <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect">
                                    <i class="mdi mdi-emoticon-poop"></i>
                                    <span> Icons </span>
                                    <span class="float-right">
                                        <i class="mdi mdi-chevron-right"></i>
                                    </span>
                                </a>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="icons-material.html">Material Design</a>
                                    </li>
                                    <li>
                                        <a href="icons-fontawesome.html">Font Awesome</a>
                                    </li>
                                    <li>
                                        <a href="icons-themify.html">Themify Icons</a>
                                    </li>
                                </ul>
                            </li> -->
                            <!-- <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect">
                                    <i class="mdi mdi-chart-areaspline"></i>
                                    <span> Charts </span>
                                    <span class="float-right">
                                        <i class="mdi mdi-chevron-right"></i>
                                    </span>
                                </a>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="charts-morris.html">Morris Chart</a>
                                    </li>
                                    <li>
                                        <a href="charts-chartist.html">Chartist Chart</a>
                                    </li>
                                    <li>
                                        <a href="charts-chartjs.html">Chartjs Chart</a>
                                    </li>
                                    <li>
                                        <a href="charts-flot.html">Flot Chart</a>
                                    </li>
                                    <li>
                                        <a href="charts-c3.html">C3 Chart</a>
                                    </li>
                                    <li>
                                        <a href="charts-xchart.html">X Chart</a>
                                    </li>
                                    <li>
                                        <a href="charts-other.html">Jquery Knob Chart</a>
                                    </li>
                                </ul>
                            </li> -->
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- end sidebarinner -->
            </div>