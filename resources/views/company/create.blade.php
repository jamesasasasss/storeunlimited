@extends('layouts.master')
@section('main_body')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="btn-group float-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                    <li class="breadcrumb-item"><a href="#">Fortune Wear</a></li>
                    <li class="breadcrumb-item active">Companies</li>
                </ol>
            </div>
            <h4 class="page-title">Add Company</h4>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="row">
    <div class="col-6">
        <div class="card m-b-30">
            <div class="card-body">            
                <h4 class="mt-0 header-title"></h4>
            </div>
            {{ Form::open(array('route' => 'company.store', 'method' => 'POST', 'class' => 'form-horizontal form-label-left')) }}
            <div class="col-md-12">
                <div class="form-group">
                    {{Form::label('code','Company Code')}}
                    <input type="text" class="form-control" name="company_code" required="">
                </div>
                <div class="form-group">
                    {{Form::label('name','Company Name')}}
                    <input type="text" class="form-control" name="company_name" required="">
                </div>
                <div class="form-group">
                    <button class="btn btn-primary btn-xs" type="submit"> Save</button>
                    <a href="{{route('company.index')}}" class="btn btn-xs btn-warning"> Cancel</a>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>
@endsection