@extends('layouts.master')
@section('main_body')
<div class="row">
    <div class="col-lg-12 animated flash">
        <?php if (session('is_success')): ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
                <center><p style="color:white">Branch was successfully updated!<i class="fa fa-check"></i></p></center>                
            </div>
        <?php endif;?>
        <?php if (session('is_added')): ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
                <center><p style="color:white">Branch was successfully added!<i class="fa fa-check"></i></p></center>                
            </div>
        <?php endif;?>
        <?php if (session('is_deleted')): ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
                <center><p style="color:white">Branch was successfully deleted!<i class="fa fa-check"></i></p></center>                
            </div>
        <?php endif;?>
    </div>
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="btn-group float-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                    <li class="breadcrumb-item"><a href="#">Fortune Wear</a></li>
                    <li class="breadcrumb-item active">Branches</li>
                </ol>
            </div>
            <h4 class="page-title">Branches</h4>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="row">
	<div class="col-12">
        <div class="card m-b-30">
            <div class="card-body">            
                <h4 class="mt-0 header-title"><a href="{{route('branch.add')}}" class="btn btn-xs btn-success"> Add Branch</a></h4>        
                <table id="datatable" class="table table-bordered">
                	<thead>
                		<tr>
                			<th>Branch Code</th>
                			<th>Branch Name</th>
                			<th>Action</th>
                		</tr>
                	</thead>
                	<tbody>
                		@foreach($branches as $branch)
                		<tr>
                			<td>{{$branch->branch_code}}</td>
                			<td>{{$branch->branch_name}}</td>
                			<td>
                				<a class="btn btn-primary btn-xs" href="{{ route('branch.show', $branch->id)}}">Edit</a>
                                {{Form::open(array('route'=>'branch.delete', 'method' => 'POST', 'class' => 'form-horizontal form-label-left'))}}
                                    {{Form::hidden('id', $branch->id)}}
                                    <button class="btn btn-xs btn-danger"> Delete</button>
                                {{Form::close()}}
                			</td>
                		</tr>
                		@endforeach
                	</tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection