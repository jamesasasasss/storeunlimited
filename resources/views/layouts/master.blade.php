<!DOCTYPE html>
<html lang="en">

    
<!-- Mirrored from mannatthemes.com/urora/vertical/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Jun 2018 01:51:42 GMT -->
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Store Unlimited System - Fortunewear</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Mannatthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="/assets/plugins/fullcalendar/vanillaCalendar.css"/>
        <link rel="stylesheet" href="/assets/plugins/jvectormap/jquery-jvectormap-2.0.2.css">
        <link rel="stylesheet" href="/assets/plugins/chartist/css/chartist.min.css">
        <link rel="stylesheet" href="/assets/plugins/morris/morris.css">
        <link rel="stylesheet" href="/assets/plugins/metro/MetroJs.min.css">

        <link rel="stylesheet" href="/assets/plugins/carousel/owl.carousel.min.css">
        <link rel="stylesheet" href="/assets/plugins/carousel/owl.theme.default.min.css">

        <link rel="stylesheet" href="/assets/plugins/animate/animate.css" type="text/css">
        <link rel="stylesheet" href="/assets/css/bootstrap-material-design.min.css" type="text/css">
        <link rel="stylesheet" href="/assets/css/icons.css" type="text/css">
        <link rel="stylesheet" href="/assets/css/style.css" type="text/css">
        <!-- DataTables -->
        <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <!-- date range picker -->
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css">
    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner"></div>
            </div>
        </div>
        <!-- Begin page -->
        <div id="wrapper">
            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
                    <i class="mdi mdi-close"></i>
                </button>

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <!--<a href="index.html" class="logo"><i class="mdi mdi-assistant"></i> Urora</a>-->
                        <a href="{{route('dashboard.index')}}" class="logo">
                            <strong style="color: white"><i class="fa fa-cubes fa-1x"></i> STORE UNLIMITED</strong>
                        </a>
                    </div>
                </div>
                @include('includes/sidebar')
            <!-- Left Sidebar End -->
            <!-- Start right Content here -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <!-- Top Bar Start -->
                    @include('includes/topbar')
                    <!-- Top Bar End -->
                    <div class="page-content-wrapper dashborad-v">
                        <div class="container-fluid">
                            @section('main_body')

                            @show
                        </div>
                    </div>
                    <!-- Page content Wrapper -->
                </div>
                <!-- content -->

                <footer class="footer">
                    © 2018 Chase Technologies Corporation - Store Unlimited System.
                </footer>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
        <script src="/assets/js/jquery.min.js"></script>
        <script src="/assets/js/popper.min.js"></script>
        <script src="/assets/js/bootstrap-material-design.js"></script>
        <script src="/assets/js/modernizr.min.js"></script>
        <script src="/assets/js/detect.js"></script>
        <script src="/assets/js/fastclick.js"></script>
        <script src="/assets/js/jquery.slimscroll.js"></script>
        <script src="/assets/js/jquery.blockUI.js"></script>
        <script src="/assets/js/waves.js"></script>
        <script src="/assets/js/jquery.nicescroll.js"></script>
        <script src="/assets/js/jquery.scrollTo.min.js"></script>


        <script src="/assets/plugins/carousel/owl.carousel.min.js"></script>
        <script src="/assets/plugins/fullcalendar/vanillaCalendar.js"></script>
        <script src="/assets/plugins/peity/jquery.peity.min.js"></script>
        <script src="/assets/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
        <script src="/assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <script src="/assets/plugins/chartist/js/chartist.min.js"></script>
        <script src="/assets/plugins/chartist/js/chartist-plugin-tooltip.min.js"></script>
        <script src="/assets/plugins/metro/MetroJs.min.js"></script>
        <script src="/assets/plugins/raphael/raphael.min.js"></script>
        <script src="/assets/plugins/morris/morris.min.js"></script>
        <script src="/assets/pages/dashborad.js"></script>

        <!-- App js -->
        <script src="/assets/js/app.js"></script>
        <!-- Data Tables -->
        <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
        <script src="/assets/plugins/datatables/jszip.min.js"></script>
        <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="/assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="/assets/plugins/datatables/buttons.colVis.min.js"></script>
        <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="/assets/plugins/datatables/responsive.bootstrap4.min.js"></script>
        <script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <script src="/assets/pages/datatables.init.js"></script>
        @stack('scripts')
    </body>
</html>